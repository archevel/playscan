from CommandParser import CommandParser	
import unittest
import math

class TestCommandParser(unittest.TestCase):

    def setUp(self): 
        self.parser = CommandParser()

    def test_parsesAdd50(self):
        fun = self.parser.parse('ADD 50')
        self.assertEqual(fun(0), 50)
        self.assertEqual(fun(-1), 49)
        self.assertEqual(fun(1), 51)

    def test_parsesAdd0(self):
        fun = self.parser.parse('ADD 0')
        self.assertEqual(fun(0), 0)
        self.assertEqual(fun(-1), -1)
        self.assertEqual(fun(1), 1)

    def test_parsesSub8(self):
        fun = self.parser.parse('SUB 08')
        self.assertEqual(fun(0), -8)
        self.assertEqual(fun(-1), -9)
        self.assertEqual(fun(1), -7)
        self.assertTrue(math.isnan(fun(float('nan'))))

    def test_parsesSub0(self):
        fun = self.parser.parse('SUB 0')
        self.assertEqual(fun(0), 0)
        self.assertEqual(fun(-1), -1)
        self.assertEqual(fun(1), 1)

    def test_parsesMul89(self):
        fun = self.parser.parse('MUL 89')
        self.assertEqual(fun(0), 0)
        self.assertEqual(fun(-1), -89)
        self.assertEqual(fun(1), 89)
        self.assertEqual(fun(3), 267)
        self.assertTrue(math.isnan(fun(float('nan'))))
        self.assertEqual(fun(float('inf')), float('inf'))
        self.assertEqual(fun(float('-inf')), float('-inf'))

    def test_parsesMulNeg32(self):
        fun = self.parser.parse('MUL -32')
        self.assertEqual(fun(0), 0)
        self.assertEqual(fun(-1), 32)
        self.assertEqual(fun(1), -32)
        self.assertEqual(fun(3), -96)
        self.assertTrue(math.isnan(fun(float('nan'))))
        self.assertEqual(fun(float('inf')), float('-inf'))
        self.assertEqual(fun(float('-inf')), float('inf'))

    def test_parsesMul0(self):
        fun = self.parser.parse('MUL 0')
        self.assertEqual(fun(0), 0)
        self.assertEqual(fun(-1), 0)
        self.assertEqual(fun(1), 0)
        self.assertTrue(math.isnan(fun(float('inf'))))

    def test_parsesDiv13(self):
        fun = self.parser.parse('DIV 13')
        self.assertEqual(fun(0), 0)
        self.assertEqual(fun(-1), float(-1)/13)
        self.assertEqual(fun(1),  float(1)/13)
        self.assertEqual(fun(13), 1)
        self.assertTrue(math.isnan(fun(float('nan'))))
        self.assertEqual(fun(float('inf')), float('inf'))
        self.assertEqual(fun(float('-inf')), float('-inf'))

    def test_parsesDivNeg19(self):
        fun = self.parser.parse('DIV -19')
        self.assertEqual(fun(0), 0)
        self.assertEqual(fun(-1), float(1)/19)
        self.assertEqual(fun(1),  float(-1)/19)
        self.assertEqual(fun(19), -1)
        self.assertTrue(math.isnan(fun(float('nan'))))
        self.assertEqual(fun(float('inf')), float('-inf'))
        self.assertEqual(fun(float('-inf')), float('inf'))

    def test_parsesDiv0(self):
        fun = self.parser.parse('DIV 0')
        self.assertTrue(math.isnan(fun(float(0))))
        self.assertEqual(fun(-1), float('-inf'))
        self.assertEqual(fun(1), float('inf'))
        self.assertTrue(math.isnan(fun(float('inf'))))
        self.assertTrue(math.isnan(fun(float('-inf'))))

    def test_parsesInvalidCommand(self):
        fun = self.parser.parse('INVALID COMMAND')
        self.assertIsNone(fun)

if __name__ == '__main__':
    unittest.main()
