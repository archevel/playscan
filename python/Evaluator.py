class Evaluator:

    def __init__(self, initialValue):
        self.currentValue = initialValue

    def evaluate(self, fun):
	self.currentValue = fun(self.currentValue)
