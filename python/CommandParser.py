import re

class CommandParser: 
    def __init__(self):
	self.regexesToFuns = {
       	    re.compile("ADD (-{0,1}\d+)") : self.add,
       	    re.compile("SUB (-{0,1}\d+)") : self.sub,
       	    re.compile("MUL (-{0,1}\d+)") : self.mul,
       	    re.compile("DIV (-{0,1}\d+)") : self.div,
       	    re.compile("SQR") : self.sqr,
        }

    def add(self, num): return lambda x: x + num

    def sub(self, num): return lambda x: x - num

    def mul(self, num): return lambda x: x * num

    def div(self, num): 
        return lambda x: self.doDivide(x, num)
    def doDivide(self, x, num): 
	result = 0.0

        if num != 0:
            result = x / num
        elif x > 0 and x < float('inf'):
            result = float('inf')
        elif x < 0 and x > float('-inf'):
            result = float('-inf')
        else:
            result = float('nan') 

        return result


    def sqr(self): return lambda x: x * x

    def parse(self, line):
        for regex, funMaker in self.regexesToFuns.iteritems():
            m = regex.match(line)
            if m and len(m.groups()) == 1:
                return funMaker(float(m.group(1)))
	    elif m:
		return funMaker()


