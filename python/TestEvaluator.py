from Evaluator import Evaluator
import unittest

class TestEvaluator(unittest.TestCase):

    def test_initialCurrentValueIsProvidedInConstructor(self):
        evaluator = Evaluator(0)
        self.assertEqual(evaluator.currentValue, 0)

        evaluator = Evaluator(5)
        self.assertEqual(evaluator.currentValue, 5)

    def test_evaluatesAppliesValueToFunctionAndSetsValueToResult(self): 
	evaluator = Evaluator(0)
        evaluator.evaluate(lambda x: x + 1)
        self.assertEqual(evaluator.currentValue, 1)
        evaluator.evaluate(lambda x: x * 44)
        self.assertEqual(evaluator.currentValue, 44)
        evaluator.evaluate(lambda x: x - 2)
        self.assertEqual(evaluator.currentValue, 42)

    

if __name__ == '__main__':
    unittest.main()
