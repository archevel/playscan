#!/usr/bin/python

from CommandParser import CommandParser
from Evaluator import Evaluator

import sys

def checkCommandLine():
    if len(sys.argv) == 2:
        return sys.argv[1]
    else:
        print "Usage: './Playscan.py <file>' or 'python Playscan.py <file>'."

def evaluateFile(filename):
    parser = CommandParser()
    evaluator = Evaluator(0)

    with open(filename, 'r') as file:
        for line in file:
            command = parser.parse(line)
            if command:
                evaluator.evaluate(command)
            else:
                print "Could not parse command '" + line + "' in file: " + filename
                return

    print evaluator.currentValue

if __name__ == '__main__':
    filename = checkCommandLine()
    if filename:
        evaluateFile(filename)

