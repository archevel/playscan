package playscan

import org.specs2.mutable._
import java.io._

class FileLineIteratorFactorySpec extends Specification {

	val factory = new FileLineIteratorFactory

	"A FileLineIteratorFactory" should {
		"creates an iterator from resoure" in {
			val testFile = new File(getClass.getResource("/testLazyFileStreamFactory.txt").toURI); 
			val iterator = factory.read(testFile.getAbsolutePath)
			
			iterator.toList must have size(1)
		}
		"throw an exception when reading nonexistent file" in {
			var nonExistentFile = new File("nonexistentfile")
			factory.read(nonExistentFile.getAbsolutePath) must throwAn[FileNotFoundException]
		}
		
	}

}
