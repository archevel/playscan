package playscan

import org.specs2.mutable._

class EvaluatorSpec extends Specification {


	"An Evaluator" should {
		"evaluate commands and return a Double" in {
			val evaluator = new Evaluator
			val func = {x:Double => x}
			val funcs = func :: Nil
			val res = evaluator.evaluate(funcs.toIterator)
			res mustEqual 0
		}
		"evaluate a several of commands" in {
			val evaluator = new Evaluator
			val double = {x:Double => x + x}
			val plus3 = {x:Double => 3 + x}
			val funcs = double :: plus3 :: double :: Nil
			val res = evaluator.evaluate(funcs.toIterator)
			res mustEqual 6
		}

	}

}
