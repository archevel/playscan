package playscan

import org.specs2.mutable._

class CommandParserSpec extends Specification {

	"A CommandParser" should {
		"parse strings to commands" in {
			val identityFunc = {x:Double => x}
			val regex = """IDENTITY (\d+)""".r
			val validCommands:PartialFunction[String, Double => Double] = { case regex(_) => identityFunc }
			val parser = new CommandParser(validCommands)
			
			parser.parse("IDENTITY 3") must be equalTo(identityFunc)
		}
		"throw an exception if command is not among the valid commands" in {
			val identityFunc = {x:Double => x}
			val regex = """IDENTITY (\d+)""".r
			val validCommands:PartialFunction[String, Double => Double] = { case regex(_) => identityFunc }
			val parser = new CommandParser(validCommands)
			
			parser.parse("UNKNOWN COMMAND") must throwA[RuntimeException](
				message = "'UNKNOWN COMMAND' is not a recognized command")
		}
		"be able to use commands in DefaultCommands which should contain all specified functions" in {

			val parser = new CommandParser(DefaultCommands.commands)
			
			val add3Func = parser.parse("ADD 3")
			add3Func(0) mustEqual 3
			add3Func(9) mustEqual 12

			val addNeg3Func = parser.parse("ADD -3")
			addNeg3Func(0) mustEqual -3
			addNeg3Func(9) mustEqual 6

			val sub9Func = parser.parse("SUB 9")
			sub9Func(0) mustEqual -9
			sub9Func(18) mustEqual 9

			val subNeg9Func = parser.parse("SUB -9")
			subNeg9Func(0) mustEqual 9
			subNeg9Func(18) mustEqual 27

			val multi31Func = parser.parse("MUL 31")
			multi31Func(0) mustEqual 0
			multi31Func(3) mustEqual 93

			val multiNeg31Func = parser.parse("MUL -31")
			multiNeg31Func(0) mustEqual 0
			multiNeg31Func(3) mustEqual -93

			val div4Func = parser.parse("DIV 4")
			div4Func(0) mustEqual 0
			div4Func(3) mustEqual 3.0d/4.0d

			val divNeg4Func = parser.parse("DIV -4")
			divNeg4Func(0) mustEqual 0
			divNeg4Func(3) mustEqual 3.0d/(-4.0d)

			val div0Func = parser.parse("DIV 0")
			div0Func(0).isNaN must beTrue
			div0Func(3) mustEqual Double.PositiveInfinity


			val sqrFunc = parser.parse("SQR")
			sqrFunc(0) mustEqual 0d
			sqrFunc(3) mustEqual 9		
			sqrFunc(-3) mustEqual 9

		}

	}

}
