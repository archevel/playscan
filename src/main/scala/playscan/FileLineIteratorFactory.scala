package playscan

import scala.io.Source

class FileLineIteratorFactory {

	def read(path: String) = Source.fromFile(path).getLines

}
