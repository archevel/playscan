package playscan

object Playscan extends App {

	if (args.size != 1) {
	
		println("Usage: scala Playscan <path-to-file>")
		System.exit(0)
	}

	val factory = new FileLineIteratorFactory
	val parser = new CommandParser(DefaultCommands.commands)
	val evaluator = new Evaluator

	try {
		val lines = factory.read(args(0))
		var commands = lines map {
			case line => parser.parse(line)
		}
		val result = evaluator.evaluate(commands)

		println(result)
	
	} catch {
		case e:Exception => println(e.getMessage); System.exit(1)
	}

}
