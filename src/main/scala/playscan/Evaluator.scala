package playscan

class Evaluator {

	def evaluate(commands: TraversableOnce[Double => Double]) = commands.foldLeft(0d){
		case (acc, func) => func(acc)
	}

}
