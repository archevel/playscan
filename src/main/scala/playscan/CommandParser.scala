package playscan

object DefaultCommands {
	private val add = """ADD (-{0,1}\d+)""".r
	private val sub = """SUB (-{0,1}\d+)""".r
	private val div = """DIV (-{0,1}\d+)""".r
	private val mult = """MUL (-{0,1}\d+)""".r
	private val sqr = """SQR""".r

	implicit val commands:PartialFunction[String, Double => Double] = {
		case add(n) => x => x + n.toDouble
		case sub(n) => x => x - n.toDouble
		case div(n) => x => x / n.toDouble
		case mult(n) => x => x * n.toDouble
		case sqr() => x => x * x
	}
}

class CommandParser(validCommands:PartialFunction[String, Double => Double]) {

	private val invalidCommand:PartialFunction[String, Double => Double] = { 
		case cmd:String => throw new RuntimeException("'" + cmd + "' is not a recognized command") 
	}

	private val matchers = validCommands orElse invalidCommand

	def parse(str: String) = matchers(str) 
	
}
